# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'markdown_macros/version'

Gem::Specification.new do |spec|
  spec.name          = "markdown_macros"
  spec.version       = MarkdownMacros::VERSION
  spec.authors       = ["Steve Ivy"]
  spec.email         = ["steveivy@gmail.com"]
  spec.description   = %q{Macro engine for Markdown content in Jekyll}
  spec.summary       = %q{Jekyll content macro engine}
  spec.homepage      = "https://gitlab.com/steveivy/jekyll-markdown-macros"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.metadata['allowed_push_host'] = 'http://rubygems.org'

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'jekyll'
  spec.add_development_dependency 'yard'
end
