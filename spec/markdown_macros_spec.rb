# :nodoc: all
require 'spec_helper'
require 'pp'

def test_method_macro(arg_h, payload)
  "method_macro output"
end


describe MarkdownMacros do

  it "has a version number" do
    expect(MarkdownMacros::VERSION).not_to be nil
  end

  describe "registers macros" do
    subject { MarkdownMacros::MacroProcessor.new }

    it "registers a macro block" do
      subject.register_block "test" do |arg_h, payload|
        "test macro output"
      end

      expect(subject.registry).to have_key("test")
    end

    it "registers a macro method" do
      subject.register "method_test", method(:test_method_macro)

      expect(subject.registry).to have_key("method_test")
    end
  end


  describe "process_macros" do
    subject { MarkdownMacros::MacroProcessor.new }

    let(:post) {
      post =  double("post")
      allow(post).to receive(:content).and_return(
        'No macros registered [[ macro ]]')
      post
    }
    let(:payload) {
      payload = double("payload")
    }
    let(:output) { subject.process_macros(post, payload) }

    it "leaves unknown macros in place" do
      expect(output).to eq post.content
    end
  end

  describe "process_macros with a registered macro" do
    subject { MarkdownMacros::MacroProcessor.new }

    let(:post) {
      post =  double("post")
      allow(post).to receive(:content).and_return(
        'Test macro registered [[ macro ]]')
      post
    }
    let(:payload) {
      payload = double("payload")
    }

    # let(:_) {
    #   subject.register "test" do |arg_h, payload|
    #     "test macro output"
    #   end
    #   puts subject.registry
    # }

    # let(:output) { subject.process_macros(post, payload) }

    it "replaces macro string with output of the macro" do
      subject.register_block "macro" do |arg_h, payload|
        "test macro output"
      end

      expect(
        subject.process_macros(
          post, payload)).to include("test macro output")
    end

  end

end
